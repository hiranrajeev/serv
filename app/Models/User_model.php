<?php

namespace Models;

use Core\Error;

class User_model extends \Core\Model {

    public function __construct() {

        parent::__construct();
    }

    public function getAllitems() {

        return $this->db->select('SELECT * FROM item_list');
    }

    public function getnames($name) {

        return $this->db->select('SELECT * FROM item_list WHERE item_name LIKE :name', array(':name' => $name));
    }
    
    public function insertitem($item){
        try {
            $this->db->insert('itemsale',$item);
        } catch (\PDOException $ex) {
            echo Error::display(''.$ex);
        }
    }

    public function getAllcomp() {

        return $this->db->select('SELECT * FROM company');
    }

    public function insertClient($data) {

        try {

            $this->db->insert('form', $data);

            return 'Registration is Successful!';
        } catch (\PDOException $ex) {

            echo Error::display('Registration Failed. ' . $ex);
        }
    }

}
