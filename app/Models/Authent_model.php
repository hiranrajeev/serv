<?php

namespace Models;

class Authent_model extends \Core\Model {

    public function getPassword($name) {
        $data = $this->db->select("SELECT * FROM login WHERE username=:name", array(':name' => $name));

        return $data[0]->password;
    }

    public function getID($name) {
        $data = $this->db->select("SELECT id FROM login WHERE username=:name", array(':name' => $name));

        return $data[0]->id;
    }

    public function getType($data) {
        $val = $this->db->select("SELECT type FROM login WHERE id=:id", array(':id' => $data));
        return $val[0]->type;
    }

}


