<?php

namespace Models;

class Common_Model extends \Core\Model {
    
    public function getService($set){
        $value=$this->db->select("SELECT * FROM item_list where item_name=:sn",array(':sn'=>$set));
        return $value;
    }
    
    public function getaddr($set){
        $value=$this->db->select("SELECT * FROM company where comp_name=:sn",array(':sn'=>$set));
        return $value;
    }
}