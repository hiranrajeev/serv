<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">

            <label class="navbar-brand">SERVICE-USER</label>

        </div>
        <!-- /.navbar-header -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">                       
                    <li>
                        <a href="home"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
                    </li>  
                    
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <a href="logout" type="button" class="btn btn-danger btn-sm pull-right" style="margin: 15px;">
            <span class="glyphicon glyphicon-log-out"></span> Logout
        </a>
        <!-- /.navbar-static-side -->
    </nav>

