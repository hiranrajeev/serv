<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Service</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php

                use Core\Error;

if ($error) {
                    echo Error::display($error);
                } else if ($data['success']) {
                    echo '<div class="alert alert-success"><strong>' . $data['success'] . '</strong></div>';
                }
                ?>

                <div class="panel-heading">
                    Registration Form
                </div>

                <div class="panel-body">
                    <form role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">


                                <fieldset>
                                    <div class="form-group">
                                        <label>DATE OF SERVICE :</label>
                                        <input class="form-control" type="date" name="date" required="" value="<?php echo date('Y-m-d') ?>" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label>COMPANY NAME :</label>
                                        <input class="form-control comp_name" placeholder="company name" name="comp_name" id="comp_name" type="text" >
                                    </div>
                                    <div class="form-group">
                                        <label>CUSTOMER NAME :</label>
                                        <input class="form-control" placeholder="customer name" name="cust_name" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label>COMPANY ADDRESS :</label>
                                        <textarea rows="4" input class="form-control" placeholder="customer address" name="cont_address" id="comp_addr" ></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>CONTACT NO :</label>
                                        <input class="form-control cont_no" placeholder="contact number" name="cont_no" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label>EMAIL :</label>
                                        <input class="form-control" placeholder="email id" name="email" type="email">
                                    </div>
                            </div>
                            <!-- /.panel-body -->



                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>SI No</th>
                                        <th>Item</th>
                                        <th>Item description</th>
                                        <th>Amount</th>
                                        <th>GST</th>
                                        <th>Final amount</th>

                                    </tr>
                                </thead>
                                <tbody class="detail">
                                    <tr class="tr">
                                        <td class="no">1</td>

                                        <td><input type="text" class="form-control item"  name="item" list="name">

                                            <datalist id="name">

                                                <?php
                                                //if ($data['name']) {
                                                //foreach ($data['name'] as $dat) {
                                                // echo '<option>' . $dat->item_name . '</option>';
                                                // }
                                                // }
                                                // 
                                                ?>

                                            </datalist>

                                        </td>                                            

                                        <td><input type="text" class="form-control Item_description" name="item_des"></td> 
                                        <td><input type="text" class="form-control Amount" value="" name="amount"></td> 
                                        <td><input type="text" class="form-control GST" value="0" name="gst"></td>
                                        <td><input type="text" class="form-control F_amount" name="F_amount"></td>
                                        <th><input type="button" class="btn btn-success" id="add" value="Add"></th>
                                        <th><input type="button" class="remove btn btn-danger" id="remove" value="Remove"></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                            <div class="col-md-4">
                            </div>

                            <div class="col-md-2">         
                                <label>Number of items :   </label>
                                <input type="text" class="form-control ns" name="ns" id="ns" value="1" readonly="">                
                            </div>
                            <div class="col-md-2">         
                                <label>Gross amount :   </label>
                                <input type="text" class="form-control" name="gtotal" id="gtotal" value="0">                
                            </div>
                            <div class="col-md-2">   
                                <label>discount : </label>
                                <input type="number" class="form-control" name="discount" id="discount" value="0">                                 
                            </div>
                            <div class="col-md-2">   
                                <label>Net amount : </label> 
                                <input type="text" class="form-control " name="ntotal" id="ntotal" value="0">

                            </div>




                        </div>

                        <span class="input-group-btn">

                            <div>
                                <!-- Change this to a button or input when using this as a form -->
                                <div class="pull-right col-sm-offset-4 col-md-3 margin-top: 100px;" >
                                    <button type="submit" name="submit" class="btn btn-success">Save</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                                <br>


                            </div>
                        </span>
                    </form> 
                </div>

            </div>
            <!-- /.panel -->       



        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

<style>
    .ui-menu {
        position: relative;
        display: inline-block;
        display: none;
        position: absolute;
        background-color: #f6f6f6;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 1;
        list-style: none;

    }
    .ui-menu .ui-menu-item {
        color: #000000;
        cursor: pointer;
        padding: 5px;

    }
    .ui-menu .ui-menu-item:hover {
        background-color: skyblue;
    }

</style>

<script>

    var allitems =<?php echo $data['name1']; ?>;
    var allcomp =<?php echo $data['cname1']; ?>;

    $('body').delegate('#comp_name', 'change', function () {
        //var parent = $(this).parent().parent();
        $.post('company', {'comp_name': $(this).val()}, function (data) {
            console.log(data);
            data = $.parseJSON(data);


            //if (data.length > 0) {
            $('#comp_addr').val(data[0].comp_addr);
            $('#comp_addr').prop('readonly', true);
            //}

        });
    });

    $(document).ready(function () {
        $('#comp_name').autocomplete({
            source: allcomp
        });
    });

    $('.cont_no').change(function () {
        var d = $('.cont_no').val();
        if (/^\d{10}$/.test(d)) {
            // value is ok, use it.
        } else {
            alert("Invalid mobile number")
            $('.cont_no').focus();
            $('.cont_no').val(null);
        }
    });
    function addnewrow() {
        var n = ($('.detail tr').length - 0) + 1;
        var tr = '<tr class="tr">' +
                '<td class="no">' + n + '</td>' +
                '<td><input type="text" class ="form-control Item" name="item"></td> ' +
                '<td><input type="text" class ="form-control Item_description"></td> ' +
                '<td><input type="text" class="form-control Amount" required></td>' +
                '<td><input type="text" class="form-control GST" required value="0"></td>' +
                '<td><input type="text" class="form-control F_amount"required></td>' +
                '</tr>';
        $('.detail').append(tr);
        $('.ns').prop('readonly', false);
        $('.ns').val(n);
        $('.ns').prop('readonly', true);
        $('.item').autocomplete({
            source: allitems
        });
    }

    $(document).ready(function () {

        $('.item').autocomplete({

            source: allitems
        });
    });
    $('.F_amount').blur(function () {
        $('#add').click();
    });
    $(function () {
        $('#add').click(function () {
            var n = $('.tr').last();
            var no = n.find('.F_amount').val();
            if (no) {
                addnewrow();
                $('.item').autocomplete({
                    source: allitems
                });
                var tr = $(this).parent().parent();
                var n = tr.find('.no').val();
                n = n + 1;
                tr.find('.no').val(n);
                $('.F_amount').blur(function () {
                    $('#add').click();
                });
            } else {
                alert('Fill all columns of current row!!!');
            }

        });
    });
    function gtotal() {
        var t = 0;
        $('.F_amount').each(function (i, e) {

            var amt = $(this).val() - 0;
            t += amt;
        });
        $('#gtotal').prop('readonly', false);
        $('#gtotal').val(t);
        $('#gtotal').prop('readonly', true);
        $('#ntotal').prop('readonly', false);
        $('#ntotal').val(t);
        $('#ntotal').prop('readonly', true);
    }

    $('body').delegate('.Amount,.GST', 'change', function () {
        var tr = $(this).parent().parent();
        var price = tr.find('.Amount').val();
        var gst = tr.find('.GST').val();
        var tax = (gst / 100) * price;
        var total = parseInt(price) + parseInt(tax);
        tr.find('.F_amount').val(total);
        $('.F_amount').prop('readonly', true);
        gtotal();
    });
    //applying discount
    $('body').delegate('#discount', 'keyup', function () {
        var d = $('#discount').val();
        var gt = $('#gtotal').val();
        $('#ntotal').val(gt - d);
    });
    $('body').delegate('.item', 'change', function () {
        var parent = $(this).parent().parent();
        $.post('item', {'item': $(this).val()}, function (data) {
            data = $.parseJSON(data);
            console.log(data);
            console.log(data[0].gst);
            //if (data.length > 0) {
            parent.find('.GST').val(data[0].gst);
            parent.find('.GST').prop('readonly', true);
            //}

        });
    });
    $('body').delegate('.remove', 'click', function () {
        var n = $('.ns').val();
        if (n > 1) {
            $('.tr').last().remove();
            n = n - 1;
            $('.ns').prop('readonly', false);
            $('.ns').val(n);
            $('.ns').prop('readonly', true);
        }
        gtotal();
    });






</script>








