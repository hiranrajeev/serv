<?php

/**
 * Sample layout.
 */
use Helpers\Assets;
use Helpers\Hooks;
use Helpers\Url;

//initialise hooks
$hooks = Hooks::get();
?>
<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
    <head>

        <!-- Site meta -->
        <meta charset="utf-8">
        <?php
        //hook for plugging in meta tags
        $hooks->run('meta');
        ?>
        <title><?php echo $data['title'] . ' - ' . SITETITLE; //SITETITLE defined in app/Core/Config.php        ?></title>

        <!-- CSS -->
       
      

       

        <?php
        Assets::css([
            Url::templatePath() . 'css/style.css',
            Url::templatePath() . 'css/sb-admin-2.css',
            Url::templatePath() . 'css/sb-admin-2.min.css',
            Url::templatePath() . 'css/animate.css',
            Url::templatePath() . 'css/bootstrap.min.css',
        ]);
        ?>
         <!-- MetisMenu CSS -->
         <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- JS -->  

        <?php
        Assets::js([
            Url::templatePath() . 'js/jquery.js',
            Url::templatePath() . 'js/jqui.js',
          
            Url::templatePath() . 'js/sb-admin-2.js',
            Url::templatePath() . 'js/sb-admin-2.min.js',
           
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
        ]);
        //hook for plugging in css
        $hooks->run('css');
        ?>
  
        <!-- Metis Menu Plugin JavaScript -->
        <script src="../vendor/metisMenu/metisMenu.min.js"></script>
        <!-- Morris Charts JavaScript -->
        <script src="../vendor/raphael/raphael.min.js"></script>
        <script src="../vendor/morrisjs/morris.min.js"></script>

    </head>
    <body>
        <?php
//hook for running code after body tag
        $hooks->run('afterBody');
        ?>


