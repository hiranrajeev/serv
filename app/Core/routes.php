<?php

/** Create alias for Router. */
use Core\Router;
use Helpers\Hooks;

/* Define routes. */

Router::any('', 'Controllers\Authent@login');
Router::any('logout', 'Controllers\Authent@logout');

Router::any('admin/home', 'Controllers\Admin@home');
Router::any('admin/logout', 'Controllers\Authent@logout');

Router::any('user/home', 'Controllers\User@home');
Router::any('user/logout', 'Controllers\Authent@logout');

Router::any('user/item','Controllers\Common@service');
Router::any('user/company','Controllers\Common@company');






/* Router::get('session', function() {
  echo '<pre>';
  print_r(\Helpers\Session::display());
  echo '</pre>';
  });



  /* Module routes.
  $hooks = Hooks::get();
  $hooks->run('routes'); */

/* If no route found. */
Router::error('Core\Error@index');





/* Turn on old style routing. */
Router::$fallback = false;

/* Execute matched routes. */
Router::dispatch();


Router::get('test', function() {
    echo 'hi';
});
