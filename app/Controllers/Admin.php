<?php
namespace Controllers;

use \Core\Controller,
    \Core\View,
    \Helpers\Session,
    \helpers\Password,
    \helpers\Url;

class Admin extends Controller{
    
   public function __construct() {

        parent::__construct();

        $this->_model = new \Models\User_model();

    }
    
    public function home() {
        echo 'welcome to admin page';
    }
    
}

