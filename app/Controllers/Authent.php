<?php

namespace Controllers;

use \Core\Controller,
    \Core\View,
    \Helpers\Session,
    \helpers\Password,
    \helpers\Url;

class Authent extends Controller {
    
    public function __construct() {

        parent::__construct();

        $this->_model = new \Models\Authent_model();

    }


    public function login() {
        
        
        View::renderTemplate('header',array('title'=>'login'));
        
        if (isset($_POST['submit'])) {

            $name = $_POST['name'];

            $password = $_POST['password'];

            //validation

            if ($password == $this->_model->getPassword($name) && $name && $password) {

                $where = array("id" => $this->_model->getID($name));

                $type = $this->_model->getType($where['id']);



                if ($type == 1) {

                    Session::set('admin', true);
                   
                    Url::redirect('admin/home');

                } else if($type == 0) {

                    Session::set('user', true);

                    Url::redirect('user/home');

                }
                
                else{
                    Url::redirect();
                }

            } else {

                $error[] = 'Wrong username or password';

            }

        }

        View::render('login',$data,$error);
        View::renderTemplate('footer');
    }

    public function logout() {
        Session::destroy();
        Url::redirect();
    }

}
