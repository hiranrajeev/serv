<?php

namespace Controllers;

use \Core\Controller,
    \Core\View,
    \Helpers\Session,
    \helpers\Password,
     \Core\Error,   
    \helpers\Url;

class User extends Controller {

    public function __construct() {

        parent::__construct();

        $this->_model = new \Models\User_model();
        $this->com = new \Controllers\Common();
    }

    public function loadHeader($title) {

        View::renderTemplate('header', array('title' => $title));

        View::render('common/navuser', array('title' => 'service'));
    }

    public function home() {
        $this->loadHeader('Home');


        if (isset($_POST['submit'])) {
            
            $phoneNumber = $_POST['cont_no'];
            if ($this->com->validate_mobile($phoneNumber)) {
            $data = array(
                'date' => $_POST['date'],
                'comp_name' => $_POST['comp_name'],
                'cust_name' => $_POST['cust_name'],
                'cont_address' => $_POST['cont_address'],
                'cont_no' => $_POST['cont_no'],
                'email' => $_POST['email'],
                'no_of_items' => $_POST['ns'],
                'gross_amount' => $_POST['gtotal'],
                'discount' => $_POST['discount'],
                'net_amount' => $_POST['ntotal'],
            );
            
            $item = array(
                'date' => $_POST['date'],
                'comp_name' => $_POST['comp_name'],
                'cont_no' => $_POST['cont_no'],
                'item_name' => $_POST['item'],
                'item_des' => $_POST['item_des'],
                'amount' => $_POST['amount'],
                'gst' => $_POST['gst'],
                'final_amount' => $_POST['F_amount'],
            );
            }
            else
            {
                $error[]="Invalid mobile number !!";
            }
        }

        if ($data) {

            $success = $this->_model->insertClient($data);
        }
        if($item){
            $this->_model->insertitem($item);
        }

        $cname = $this->_model->getAllcomp();
        $cname1 = array();
        foreach ($cname as $key => $s)
            $cname1[$key] = $s->comp_name;

        $name = $this->_model->getAllitems();
        $name1 = array();
        foreach ($name as $key => $s)
            $name1[$key] = $s->item_name;



        View::render('user\home', array('success' => $success, 'name' => $name, 'name1' => json_encode($name1),'cname1' => json_encode($cname1)),$error);
        View::renderTemplate('footer');
    }

}
