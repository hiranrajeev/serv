<?php

namespace Controllers;

class Common extends \Core\Controller {

    private $_model;

    public function __construct() {
        parent::__construct();
        $this->_model = new \Models\Common_Model();
    }
    public function service() {
        if (isset($_POST['item'])) {
            $data = $this->_model->getService($_POST['item']);
           
            echo json_encode($data);
        }
    }
    
    function validate_mobile($mobile) {
        return preg_match('/^[0-9]{10}+$/', $mobile);
    }
    
    public function company() {
        if (isset($_POST['comp_name'])) {
            $data = $this->_model->getaddr($_POST['comp_name']);
           
            echo json_encode($data);
        }
    }
}

